!	This program uses RK4 algorithm to solve a system of ODEs
!	defined in the 'fsub' subroutine
!	Ethan Kung ekung@clemson.edu 2/18/2014
!   Finalized version: 
!   Akash Gupta, akashg@clemson.edu 21 Jan 2023

! NOTE: subroutine form for the lpn base code is needed for FORTRAN-Python integration.
! The output from the lpn is stored in lcycle and then fed to the python wrapper code, and the
! wrapper code will process the output data.
subroutine rk4(results,lcycle)
    implicit none
    INTEGER, PARAMETER :: nx = 60 ! num of variables
    INTEGER, PARAMETER :: no = 11 ! num of LPN outputs
!      REAL*8, DIMENSION(100000,nx), INTENT(out) :: results
    REAL*8, INTENT(out) :: results(100000,nx)
    REAL*8 output(1,no) ! nstep/interv
    REAL*8 x(nx), xref(nx), k1(nx), k2(nx), k3(nx), k4(nx)
    REAL*8 ts, nstep, interv
    INTEGER i, j, its, end_idx
    INTEGER, INTENT(out) :: lcycle
    COMMON ts, its

    INCLUDE "parameters.f90" !needed for heart rate and tvs_percent

    !     Total sim time = nstep*ts, saving resolution = interv*ts
    ts = 1.d-3 / 50.d0 !time step size
    nstep = 30000.d0 * 50.d0 !steps to simulate
    interv = 1.d0 * 50.d0 !steps between saves
    its = 1

    ! Calculate no of timesteps in last cycle
    lcycle = ceiling ( (60D0/hr)*1000D0 )

    !     This section reads in the initial values
    OPEN(unit = 1, FILE = 'initVals.f90')
    DO i = 1, nx
    READ(1,*) xref(i)
    END DO
    CLOSE(1)


    !     Initialize x
    x = xref
    
    !     RK4 algorithm
    DO i = 1,int(nstep)
        call fsub(x, nx, k1);  ! evaluate f and store into k1
        !---------------------------------
        !     Place this part here to write right after the "addition" x where
        !     evaluted for the first time, before x is changed again in the
        !     next line
        IF (mod(i,int(interv)).EQ.0.d0) THEN
        results(i / int(interv),:) = x  ! write out results
        END IF
        !----------------------------------
        x = xref + ((ts / 2.d0) * k1);
        call fsub(x, nx, k2);  ! evaluate f for k2
        x = xref + ((ts / 2.d0) * k2);
        call fsub(x, nx, k3);  ! evaluate f for k3
        x = xref + ((ts / 2.d0) * k3);
        call fsub(x, nx, k4);  ! evaluate f for k4
    
        !         Update x variables with next timestep values
        xref = xref + (ts / 6) * (k1 + (2 * k2) + (2 * k3) + k4)
        x = xref
    
    END DO
    
    !	Write out results to file. NOTE: To be commented out only for data generation code since full results not necessary.
    OPEN(2, FILE = 'results')
    DO i = 1,int(nstep / interv)
    DO j = 1,nx
        WRITE(2,'(ES18.8)',ADVANCE = "NO") results(i,j)
    END DO
    WRITE(2,*)
    END DO
    CLOSE(2)
    
end subroutine rk4

!     Subroutine to compute f(x) at each time step
subroutine fsub(x,n,f)
    implicit none
    INTEGER, INTENT(in) :: n
    !	real(kind=8), dimension(n), intent(inout) :: x
    REAL*8 x(n)
    !	Remove intent out to allow writing within the subroutine
    REAL*8, DIMENSION(n), INTENT(out) :: f
    
    INTEGER, PARAMETER :: nbr = 2 ! number of pulmonary branches
    REAL*8 Qpc(nbr)
    
    !	These are the dummy variables
    REAL*8 Tc, Tm, Tad,t, ts
    REAL*8 Pla, Plv, Pra, Prv, Plvold, Plvnew, Pao
    REAL*8 Aa, Pith, dPith, dPlv
    ! REAL*8 Peq, Req
    REAL*8 Pbar, elas, elasLV, elasRV, Rpav
    REAL*8 Qsvc, Qcrbv, Qlegc, Qlegv, Qabivc, Qthivc, Qcao
    REAL*8 Qhv, Qla, Qlv, Qka, Qkv, Qia, Qiv
    COMPLEX*8 fe(20)
    INTEGER its, k, sys, cor, extra, pul
    COMMON ts, its
    
    INCLUDE "parameters.f90"
    
    ! Raov = 1.0*12*0.01D0
    Rpav = 0D0
    
    sys   = 26 ! how many systemic variables
    cor   = 3 ! how many coronary variables
    extra = 24 ! how many extra variables to save
    pul   = sys + cor + extra + 1 ! starting index for pulmonary variables

    !----------------------------------------------------------------------!

    !---------------------------------Time---------------------------------!

    !     Time periods
    t = x(sys + cor + 1)
    f(sys + cor + 1) = 1.d0 ! clock variable to keep time
    ! remember to reset "t" definition above if changing this

    Tc  = 60.d0 / hr ! cardiac period
    Tm  = MOD(t,Tc)  ! time in cardiac period

    !----------------------------------------------------------------------!

    !--------------------------------Atria---------------------------------!

    !     Atrium Activation Functions (from GLENN equations)

    Tad = Tc - Tas ! time for atrial diastole

    IF (Tm.LE.T1) THEN
        Aa = 0.5d0 * (1.d0 - DCOS(2.d0 * pi * (Tm - T1 + Tas) / Tas))
    ELSEIF ((T1 + Tad).LE.Tm .AND. Tm.LE.Tc) THEN
        Aa = 0.5d0 * (1.d0 - DCOS(2.d0 * pi * (Tm - T1 - Tad) / Tas))
    ELSE
        Aa = 0.d0
    END IF

    !     Atrial pressure
    Pra = (Aa * (x(19) - Vra0) / CCsaR) &
        + (csaR * (DEXP(dsaR * (x(19) - Vra0)) - 1.d0))
    Pla = (Aa * (x(23) - Vla0) / CCsaL) &
        + (csaL * (DEXP(dsaL * (x(23) - Vla0)) - 1.d0))

    !-----------------------------------------------------------------------

    !------------------------------Ventricles------------------------------!

    !	Ventricular normalized elastance function, peak at 0.3/T

    !	Resting elastance shape; Fourier coefficients
    fe = (/  (0.223791662, 0.000000000), &
    ( 0.041053540 , -0.409484599), &
    (-0.231401116 , -0.026813529), &
    ( 0.017514609 ,  0.085190160), &
    ( 0.013159473 , -0.050109864), &
    (-0.047293329 ,  0.000210483), &
    (-0.001339371 ,  0.022154522), &
    (-0.000039917 , -0.005933291), &
    (-0.012594069 ,  0.002455735), &
    (-0.001121362 ,  0.009165310), &
    ( 0.001600754 ,  0.000559328), &
    (-0.002965519 ,  0.001610140), &
    ( 0.000445087 ,  0.003430950), &
    ( 0.000958265 ,  0.000409491), &
    ( 0.000004164 ,  0.000557564), &
    ( 0.000041503 ,  0.000217449), &
    (-0.000014608 ,  0.000330808), &
    (-0.000094191 ,  0.000170295), &
    (-0.000035561 ,  0.000198275), &
    (-0.000132250 ,  0.000147014) /)

    elas = 0.d0

    DO k = 1,20
        elas = elas + REAL(fe(k)) &			  !scale by Tvs
                    * DCOS(2.d0 * pi * (k - 1) * ((Tm * 0.3d0) / Tvs)) &
                    - aimag(fe(k)) &
                    * DSIN(2.d0 * pi * (k - 1) * ((Tm * 0.3d0) / Tvs))
    END DO

    !	Set elas=0 if Tc/Tvs is too large when HR is low
    !	and go over the elas waveform cycle
    IF ((Tm / Tvs) .GT. 3.33333d0) THEN  ! meaning that Tm*0.3d0/Tvs > 1
        elas = 0.d0
    END IF

    elasRV = (EmaxRV * elas) + EoffsetRV
    elasLV = (EmaxLV * elas) + EoffsetLV

    Prv = elasRV * (x(21) - Vrv0)
    Plv = elasLV * (x(25) - Vlv0)

    !	Calculate dPlv/dt for use in coronary circulation
    IF (its.EQ.1) THEN
    Plvold = 0.d0
    Plvnew = Plv
    dPlv = 0.d0
    ELSE
    Plvold = Plvnew
    Plvnew = Plv
    dPlv = (Plvnew - Plvold) / ts
    END IF

    !---------------------------------------------------------------------!

    !	Coupling Atrial Pressure with Ventricular Pressure

    Pra = Pra + (AV_couple * Prv)
    Pla = Pla + (AV_couple * Plv)

    !---------------------------------------------------------------------!

    !-------------------------Respiration Effects-------------------------!

    !     Respiration (from GLENN equations)
    !      Tr = 60.d0 / fr
    !      Tmr = MOD(t,Tr)
    !      Tsr = Trr * Tr  ! don't need this if prescribing fourier waveform
    !
    !      IF (Tmr.LT.Tsr) THEN
    !         Ar  = 0.5d0 * (1.d0 - DCOS(2.d0 * pi * Tmr / Tsr))
    !         dAr = (pi / Tsr) * DSIN(2.d0 * pi * Tmr / Tsr) ! derivative wrt Tmr
    !      ELSE
    !         Ar  = 0.d0
    !         dAr = 0.d0
    !      END IF
    !
    !      Pith  = (APith * Ar) + P0ith
    !      dPith = (APith * dAr)
    !
    !	Connect Pith to the heart pressures
    !
    !      Pra = Pra + (Pith * 0.5d0)
    !      Pla = Pla + (Pith * 0.5d0)
    !      Prv = Prv + (Pith * 0.3d0)
    !	  Plv = Plv + (Pith * 0.3d0)
    !
    !	Keeping Pith Constant

    Pith  = 0.d0
    dPith = 0.d0

    !----------------------------------------------------------------------!

    !--------------------------Pulmonary Circuit---------------------------!

    !	Calculate the pulmonary pressure drop across branches
    !	This pressure drop will be the same for all branches in a
    !	a parallel circuit

    !      Req = 1.d0 / SUM(1.d0 / Rlunga) ! Req for parallel
    !      Peq = x(22) * Req ! use P = QR for equivalent pressure drop
    !      x(pul:pul + 1)     = Peq / Rlunga(1:2) ! flow entering each branch (Qlunga)

    ! All pulmonary Flows
    x(pul) = x(22) / 2
    x(pul + 1) = x(22) / 2
    x(pul + 2:pul + 3) = (x(pul + 5:pul + 6) - Pla) / Rlungv(1:2)   ! Qlungv
    Qpc(1:2) = (x(pul:pul + 1) - x(pul + 2:pul + 3))                ! Flow into pulmonary capacitor
    x(pul + 4) = x(pul + 2) + x(pul + 3)                            ! Qlung out total

    !	Pulmonary pressures for RCR configuration  
    f(pul + 5) = (Qpc(1) / Clung(1)) + dPith + Qpc(1)*Rpav
    f(pul + 6) = (Qpc(2) / Clung(2)) + dPith + Qpc(2)*Rpav

    !	Calculate pulmonary artery junction pressure
    Pbar = x(pul + 5) + (x(pul) * Rlunga(1)) ! using first branch

    !----------------------------------------------------------------------!

    !--------------------------------Valves--------------------------------!

    !	Valve Flow
    !   Set flowrate to zero if negative

    IF (x(20).LT.0.d0) x(20) = 0.d0
    IF (x(22).LT.0.d0) x(22) = 0.d0
    IF (x(24).LT.0.d0) x(24) = 0.d0
    IF (x(26).LT.0.d0) x(26) = 0.d0

    !	Tricuspid valve
    IF (Pra.LT.Prv .AND. x(20).LE.0) THEN
        f(20) = 0.d0
        x(20) = 0.d0
    ELSE
        f(20) = (Pra - Prv - (Ktri * x(20) * x(20))) / Ltri
    END IF

    !	Pulmonary valve
    IF (Prv.LT.Pbar .AND. x(22).LE.0.d0) THEN
        f(22) = 0.d0
        x(22) = 0.d0
    ELSE      
        f(22) = (Prv - (x(22) * Rpul) &
            - (Kpul * x(22) * x(22)) - Pbar) / Lpul
    END IF

    !	Mitral valve
    IF (Pla.LT.Plv .AND. x(24).LE.0) THEN
        f(24) = 0.d0
        x(24) = 0.d0
    ELSE
        f(24) = (Pla - Plv - (Kmit * x(24) * x(24))) / Lmit
    END IF

    !	Aortic valve

    ! Define the flow leaving Lao and entering Cao, and pressure after Lao
    Qcao = (x(26) - x(3) - x(5) - x(15) - x(sys + 2) )
    Pao = (x(4) + Qcao*Raov)

    IF (Plv.LT.Pao .AND. x(26).LE.0.d0) THEN
        f(26) = 0.d0
        x(26) = 0.d0
    ELSE
        f(26) = (Plv - (x(26) * Rao) - (Kao * x(26) * x(26)) - Pao) / Lao
        ! f(26) = (Plv - (x(26) * Rao) - (Kao * x(26) * x(26)) &
        !  -  ((x(26) - x(3) - x(5) - x(15) - x(sys + 2))*Raov + x(4)) ) / Lao  
    END IF

    !----------------------------------------------------------------------!
    !     Cardiac volumes must be positive
    IF (x(19).LT.0.d0) x(19) = 0.d0
    IF (x(21).LT.0.d0) x(21) = 0.d0
    IF (x(23).LT.0.d0) x(23) = 0.d0
    IF (x(25).LT.0.d0) x(25) = 0.d0

    !-------------------------Coronary circulation-------------------------!

    !	 Pressure is coupled with Plv

    x(sys + 2) = (Pao - x(sys + 1)) / Rcora                 ! Qcora
    x(sys + 3) = (x(sys + 1) - Pra) / Rcorv                 ! Qcorv
    f(sys + 1) = ((x(sys + 2) - x(sys + 3)) / Ccor) + dPlv  ! Pcor
        
    !---------------------------Extra Variables----------------------------!

    Qsvc   = (x(1) - Pra) / Rsvc
    Qcrbv  = (x(2) - x(1)) / Rcrbv
    Qlegc  = (x(10) - x(11)) / Rlegc
    Qlegv  = (abs(x(11) - x(12)) + x(11) - x(12)) / (2 * Rlegv)
    Qabivc = (x(12) - x(13)) / Rabivc
    Qthivc = (x(13) - Pra) / Rthivc
    Qhv    = (x(14) - x(1)) / Rhv
    Qla    = (x(6) - x(16)) / Rla
    Qlv    = (x(16) - x(13)) / Rlv
    Qka    = (x(6) - x(17)) / Rka
    Qkv    = (x(17) - x(13)) / Rkv
    Qia    = (x(8) - x(18)) / Ria
    Qiv    = (x(18) - x(16)) / Riv

    !----------------------------Heart Volumes-----------------------------!

    !	Right heart volumes
    f(19) = Qsvc + Qthivc + x(sys + 3) - x(20)                  ! Vra
    f(21) = x(20) - x(22)                                       ! Vrv

    !	Left heart volumes
    f(23) = x(pul + 4) - x(24)                                  ! Vla
    f(25) = x(24) - x(26)                                       ! Vlv

    !     Assign the additional parameters to be printed
    f(sys + cor + 1)  = 1.d0 ! time (repeated from above)
    x(sys + cor + 2)  = Pra
    x(sys + cor + 3)  = Prv
    x(sys + cor + 4)  = Pla
    x(sys + cor + 5)  = Plv
    x(sys + cor + 6)  = f(20)
    x(sys + cor + 7)  = Pao ! Note replaced elas with Pao
    x(sys + cor + 8)  = elasRV
    x(sys + cor + 9)  = elasLV
    x(sys + cor + 10) = Pbar
    x(sys + cor + 11) = Pith
    x(sys + cor + 12) = Qsvc
    x(sys + cor + 13) = Qcrbv
    x(sys + cor + 14) = Qlegc
    x(sys + cor + 15) = Qlegv
    x(sys + cor + 16) = Qabivc
    x(sys + cor + 17) = Qthivc
    x(sys + cor + 18) = Qhv
    x(sys + cor + 19) = Qla
    x(sys + cor + 20) = Qlv
    x(sys + cor + 21) = Qka
    x(sys + cor + 22) = Qkv
    x(sys + cor + 23) = Qia
    x(sys + cor + 24) = Qiv
    !----------------------------------------------------------------------!

    !-------------------------Systemic Circulation-------------------------!

    f(1)  = ((Qcrbv + Qhv - Qsvc) / Csvc) + dPith               ! Psvc
    f(2)  = (x(3) - Qcrbv) / Ccrb                               ! Pcrb
    f(3)  = ((Pao - (x(3) * Rcrba)) - x(2)) / Lcrba             ! Qcrba
    ! f(61)  = ((x(26) - x(3) - x(5) - x(15) - x(sys + 2)) &	
    !       / Cao) + dPith
    f(4)  = (Qcao / Cao) + dPith                                ! Paov
    ! f(4) = (x(26) - x(3) - x(5) - x(15) - x(sys + 2))*Raov + x(61)
    f(5)  = ((Pao - (x(5) * Rthao)) - x(6)) / Lthao             ! Qthao
    f(6)  = ((x(5) - x(7) - Qla - Qka) / Cthao) + dPith         ! Pthao
    f(7)  = ((x(6) - (x(7) * Rabao)) - x(8)) / Labao            ! Qabao
    f(8)  = (x(7) - Qia - x(9)) / Cabao                         ! Pabao
    f(9)  = ((x(8) - (x(9) * Rlega)) - x(10)) / Llega           ! Qlega
    f(10) = (x(9) - Qlegc) / Clega                              ! Plega
    f(11) = (Qlegc - Qlegv) / Clegv                             ! Plegv
    f(12) = (Qlegv - Qabivc) / Cabivc                           ! Pabivc
    f(13) = ((Qabivc + Qlv + Qkv - Qthivc) / Cthivc) &          ! Pthivc
        + dPith
    f(14) = (x(15) - Qhv) / Ch                                  ! Ph
    f(15) = ((Pao - (x(15) * Rha)) - x(14)) / Lha               ! Qha
    f(16) = (Qiv + Qla - Qlv) / Cl                              ! Pl
    f(17) = (Qka - Qkv) / Ck                                    ! Pk
    f(18) = (Qia - Qiv) / Ci                                    ! Pi

    its = its + 1

end subroutine fsub

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!