# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat July 04 13:05:08 2020

@author: Akash Gupta akashg@clemson.edu
"""
import numpy as np
import pandas as ps
from pathlib import PurePath
import os
import sys
import glob

# Reading Values from input.csv---------------------------------------------------------------------------------------------
# Get the directory path from argement passed to the script
dpath = sys.argv[1]

# Find the all_inputs.csv file in that directory
flist = glob.glob(dpath+"all_inputs*.csv")
# flist is actually a list of paths, not a string, so access the first element of that list
# which is the all_inputs.csv file we need
fpath = PurePath(flist[0])

# Actually read the all_inputs.csv file
inputs = ps.read_csv(fpath)

# Access all the values in the last column
x = inputs.iloc[:, -1]

# This code is adapted from a sensitivity analysis code so, normally iter should = 1.
#iter = 1

# Set up indices for accessing the correct values in x ---------------------------------------------------------------------
# Number of Values (Change the number if the values change)
scale_init_end        = 1
initial_values_end    = 60 + scale_init_end		    # 61
heart_parameters_end  = 33 + initial_values_end	    # 94
heart_scale_end       = 33 + heart_parameters_end	# 127
system_parameters_end = 35 + heart_scale_end		# 162
system_scale_end      = 35 + system_parameters_end	# 197
system_frac_end       = 10 + system_scale_end		# 207
raov_scale_end        =  2 + system_frac_end        # 209

# Note, indices start from zero, hence the numbers are off by 1
scale_init        = x.iloc[0:scale_init_end]              		        # Scale the Initial Values
initial_values    = x.iloc[scale_init_end:initial_values_end]      	    # Initial Values
heart_parameters  = x.iloc[initial_values_end:heart_parameters_end]    	# Heart Parameters
heart_scale       = x.iloc[heart_parameters_end:heart_scale_end]        # Scaling the Heart Parameters
system_parameters = x.iloc[heart_scale_end:system_parameters_end] 		# Systemic Parameters
system_scale      = x.iloc[system_parameters_end:system_scale_end]      # Scaling the Systemic Parameters
system_frac       = x.iloc[system_scale_end:system_frac_end]       		# Resistance Fractions
raov              = x.iloc[raov_scale_end - 2]
raov_scale        = x.iloc[raov_scale_end - 1]                              # Only 1 value for Raov scale

# print(f'raov={raov}')
# print(f'raov_scale={raov_scale}')

# Process data for initVals.f90 file ---------------------------------------------------------------------------------------
# Scale the initial values here
# Have to create a new variable to store scaled values. In-place multiplication generates garbage values.
inititial_vales_scaled = ps.DataFrame()
for i in range(0, (initial_values_end - scale_init_end)-1): #Indices start from zero
    initial_values_scaled = initial_values * scale_init[0]

# Specify initVals path
init_path = PurePath(dpath)/"initVals.f90"
# Write out initVals.f90 file
initial_values_scaled.to_csv(init_path, mode = 'w', index = False, header = False, line_terminator = '\n', float_format = '%.8f')

# Process data for parameters.f90 file -------------------------------------------------------------------------------------
# Convert dataframes to numpy arrays because dealing with them is simpler
heart_parameters.to_numpy
heart_scale.to_numpy
system_parameters.to_numpy
system_scale.to_numpy
system_frac.to_numpy

# Dummy array to be concatenated to the front of the arrays so that indices start from 1 rather than 0
a = np.array([0])

# Have to use concatenate, np.insert is buggy
heart_parameters  = np.concatenate((a, heart_parameters))
heart_scale       = np.concatenate((a, heart_scale))
system_parameters = np.concatenate((a, system_parameters))
system_scale      = np.concatenate((a, system_scale))
system_frac       = np.concatenate((a, system_frac))

# Scale input parameters here
# Cardiac Cycle
hr = heart_scale[1] * heart_parameters[1]
Tc = 60 / hr

# Atrial Parameters
Tas   = ((heart_scale[2] * heart_parameters[2]) / 100) * Tc
T1    = ((heart_scale[3] * heart_parameters[3]) / 100) * Tas
Vra0  =  heart_scale[4]  * heart_parameters[4]
Vla0  =  heart_scale[5]  * heart_parameters[5]
CCsaR =  heart_scale[6]  * heart_parameters[6]
csaR  =  heart_scale[7]  * heart_parameters[7]
dsaR  =  heart_scale[8]  * heart_parameters[8]
CCsaL =  heart_scale[9]  * heart_parameters[9]
csaL  =  heart_scale[10] * heart_parameters[10]
dsaL  =  heart_scale[11] * heart_parameters[11]

# Ventricle Parameters
Tvs = ((heart_scale[12] * heart_parameters[12]) / 100) * Tc

EmaxRV    = heart_scale[13] * heart_parameters[13]
EoffsetRV = heart_scale[14] * heart_parameters[14]
EmaxLV    = heart_scale[15] * heart_parameters[15]
EoffsetLV = heart_scale[16] * heart_parameters[16]

Vrv0  = heart_scale[17] * heart_parameters[17]
Vlv0  = heart_scale[18] * heart_parameters[18]

AV_couple = heart_scale[19] * heart_parameters[19]

# Intrathoracic
fr    = heart_scale[20] * heart_parameters[20]
Trr   = heart_scale[21] * heart_parameters[21]
P0ith = heart_scale[22] * heart_parameters[22]
Apith = heart_scale[23] * heart_parameters[23]

# Valves
Ltri = heart_scale[24] * heart_parameters[24]
Lpul = heart_scale[25] * heart_parameters[25]
Lmit = heart_scale[26] * heart_parameters[26]
Lao  = heart_scale[27] * heart_parameters[27]

Ktri = heart_scale[28] * heart_parameters[28]
Kpul = heart_scale[29] * heart_parameters[29]
Kmit = heart_scale[30] * heart_parameters[30]
Kao  = heart_scale[31] * heart_parameters[31]

Rpul = heart_scale[32] * heart_parameters[32]
Rao  = heart_scale[33] * heart_parameters[33]

# Systemic Parameters
# Coronary
Ccor = system_scale[1] * system_parameters[1]

Rcor = system_scale[2] * system_parameters[2]
Rcora = system_frac[1] * Rcor
Rcorv = (1 - system_frac[1]) * Rcor

heart_list_string = np.array( ["hr", "Tas", "T1", "Vra0", "Vla0", "CCsaR", "csaR", "dsaR", "CCsaL", "csaL", "dsaL", \
    "Tvs", "EmaxRV", "EoffsetRV", "EmaxLV", "EoffsetLV", "Vrv0", "Vlv0", "AV_couple", \
    "fr", "Trr", "P0ith", "Apith", "Ltri", "Lpul", "Lmit", "Lao", \
    "Ktri", "Kpul", "Kmit", "Kao", "Rpul", "Rao", "Ccor", "Rcora", "Rcorv"] )

heart_list = np.array( [hr, Tas, T1, Vra0, Vla0, CCsaR, csaR, dsaR, CCsaL, csaL, dsaL, \
    Tvs, EmaxRV, EoffsetRV, EmaxLV, EoffsetLV, Vrv0, Vlv0, AV_couple, \
    fr, Trr, P0ith, Apith, Ltri, Lpul, Lmit, Lao, \
    Ktri, Kpul, Kmit, Kao, Rpul, Rao, Ccor, Rcora, Rcorv] )

# Cerebral Branch
Lcrba = system_scale[3] * system_parameters[3]

Ccrb  = system_scale[4] * system_parameters[4]

Rcrb  = system_scale[5] * system_parameters[5]
Rcrba = system_frac[2] * Rcrb
Rcrbv = (1 - system_frac[2]) * Rcrb

# Hands
Lha = system_scale[6] * system_parameters[6]

Ch  = system_scale[7] * system_parameters[7]

Rh  = system_scale[8] * system_parameters[8]
Rha = system_frac[3] * Rh
Rhv = (1 - system_frac[3]) * Rh

# Superior Vena Cava
Csvc = system_scale[9] * system_parameters[9]
Rsvc = system_scale[10] * system_parameters[10]

# Aorta
Cao = system_scale[11] * system_parameters[11]

Lthao = system_scale[12] * system_parameters[12]
Labao = system_scale[13] * system_parameters[13]

Cthao = system_scale[14] * system_parameters[14]
Cabao = system_scale[15] * system_parameters[15]

Rthao = system_scale[16] * system_parameters[16]
Rabao = system_scale[17] * system_parameters[17]

# Leg
Llega = system_scale[18] * system_parameters[18]

Clega = system_scale[19] * system_parameters[19]
Clegv = system_scale[20] * system_parameters[20]

Rleg  = system_scale[21] * system_parameters[21]
Rlega = system_frac[4] * Rleg
Rlegv = system_frac[5] * Rleg
Rlegc = (1 - system_frac[4] - system_frac[5]) * Rleg

# Inferior Vena Cava
Cabivc = system_scale[22] * system_parameters[22]
Cthivc = system_scale[23] * system_parameters[23]

Rabivc = system_scale[24] * system_parameters[24]
Rthivc = system_scale[25] * system_parameters[25]

# Abdomen and Viscera
Cl = system_scale[26] * system_parameters[26]
Ck = system_scale[27] * system_parameters[27]
Ci = system_scale[28] * system_parameters[28]

Rl  = system_scale[29] * system_parameters[29]
Rla = system_frac[6] * Rl
Rlv = (1 - system_frac[6]) * Rl

Rk  = system_scale[30] * system_parameters[30]
Rka = system_frac[7] * Rk
Rkv = (1 - system_frac[7]) * Rk

Ri  = system_scale[31] * system_parameters[31]
Ria = system_frac[8] * Ri
Riv = (1 - system_frac[8]) * Ri # Line 171 in Write_Input_ANN2.m

systemic_list_string = ["Lcrba", "Ccrb", "Rcrba", "Rcrbv", "Lha", "Ch", "Rha", "Rhv", \
    "Csvc", "Rsvc", "Cao", "Lthao", "Labao", "Cthao", "Cabao", "Rthao", "Rabao", \
    "Llega", "Clega", "Clegv", "Rlega", "Rlegc", "Rlegv", "Cabivc", "Cthivc", \
    "Rabivc", "Rthivc", "Cl", "Ck", "Ci", "Rla", "Rlv", "Rka", "Rkv", "Ria", "Riv"]

systemic_list = [ Lcrba , Ccrb , Rcrba , Rcrbv , Lha , Ch , Rha , Rhv , \
     Csvc , Rsvc , Cao , Lthao , Labao , Cthao , Cabao , Rthao , Rabao , \
     Llega , Clega , Clegv , Rlega , Rlegc , Rlegv , Cabivc , Cthivc , \
     Rabivc , Rthivc , Cl , Ck , Ci , Rla , Rlv , Rka , Rkv , Ria , Riv ]

# Pulmonary
Clung1 = system_scale[32] * system_parameters[32]
Clung2 = system_scale[33] * system_parameters[33]
Clist  = np.array( [Clung1, Clung2] )

Rlung1  = system_scale[34] * system_parameters[34]
Rlunga1 = system_frac[9] * Rlung1
Rlungv1 = (1 - system_frac[9]) * Rlung1
Rlung2  = system_scale[35] * system_parameters[35]
Rlunga2 = system_frac[10] * Rlung2
Rlungv2 = (1 - system_frac[10]) * Rlung2
Rlist   = np.array( [ [Rlunga1, Rlunga2], \
                    [Rlungv1, Rlungv2] ] )

Raov = raov*raov_scale

# Specify parameters.f90 path and open the file for writing
param_path = PurePath(dpath)/"parameters.f90"

with open(param_path , 'w') as f:
    # Write out parameters.f90 file
    
    # Need to do the assignment or f.write() will return the number of char. printed
    a = f.write('      REAL*8, PARAMETER :: &' + '\n') 
    
    for i in range(0, len(heart_list_string)):
        # Set up the string to be written out
        h_out = ' ' + heart_list_string[i] + ' = ' + '%.9f' % heart_list[i] + 'D0 , & ' + '\n'
        a     = f.write(h_out) 

    for i in range(0, len(systemic_list_string)):
        # Set up the string to be written out
        s_out = ' ' + systemic_list_string[i] + ' = ' + '%.9f' % systemic_list[i] + 'D0 , & ' + '\n'
        a     = f.write(s_out)
    
    # Write out pulmonary parameters line by line
    cpul_out = ' Clung(2) = (/  & \n' + \
               ' ' + '%.9f' % Clist[0] + 'D0 , & ' + '\n' + \
               ' ' + '%.9f' % Clist[1] + 'D0 /), & ' + '\n'
    a = f.write(cpul_out)
    
    rlunga_out = ' Rlunga(2) = (/  & \n' + \
               ' ' + '%.9f' % Rlist[0, 0] + 'D0 , & ' + '\n' + \
               ' ' + '%.9f' % Rlist[0, 1] + 'D0 /), & ' + '\n'
    a = f.write(rlunga_out)
    
    rlungv_out = ' Rlungv(2) = (/  & \n' + \
               ' ' + '%.9f' % Rlist[1, 0] + 'D0 , & ' + '\n' + \
               ' ' + '%.9f' % Rlist[1, 1] + 'D0 /), & ' + '\n'
    a = f.write(rlungv_out)
    
    raov_out = ' ' + 'Raov' + ' = ' + '%.9f' % Raov + 'D0 , & ' + '\n'
    a        = f.write(raov_out)
    
    a = f.write(' pi = 3.14159265D0 \n')