# This script runs the generic LPN protocol for resting conditions
# from tensorflow.keras import layers,models
# from tensorflow.keras import callbacks
import os
# Supress tensorflow output messages
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"
import tensorflow as tf
# from livelossplot import PlotLossesKerasTF
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import PowerTransformer
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import RobustScaler
import numpy as np
import pandas as ps
import datetime
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
import seaborn as sns
import joblib
import sys
from pathlib import PurePath

# Set up all constants and file paths here ######################################################################################

# NOTE: Arguments that need to be provided to the script are:
#  1. Number of predictors for the neural network
#  2. Number of quantities predicted by the neural network
#  3. Location of saved neural network model
#  4. Location of saved x scaler data
#  5. Location of saved y scaler data
#  6. location of the protocol inputs file
#  7. Iteration number of the protocol being tested
#  8. Directory Location of reference all_inputs.csv file
#  9. Directory Location at which the protocol modified all_inputs.csv file should be saved to.

# Neural Network information
num_predictors = int(sys.argv[1])
num_predicted  = int(sys.argv[2])
nn_path        = sys.argv[3]
scaler_x_path  = sys.argv[4]
scaler_y_path  = sys.argv[5]
# scaler_x_path = 'scaler_x_resting_filter_exp2.gz'
# scaler_y_path = 'scaler_y_resting_filter_exp2.gz'
# nn_path = '/home/akashg/Generic_LPN_protocol_git/gen_lpn/ResNet50_resting_filter_exp2'

# Specify location of the protocol inputs file and iteration number
input_path = sys.argv[6]
iter_num   = int(sys.argv[7])

# Paths for files modified by the neural network
ref_inputs_dir   = PurePath(sys.argv[8])
mod_inputs_dir   = PurePath(sys.argv[9])

# Obtain the protocol inputs
protocol_inputs = ps.read_csv(filepath_or_buffer = input_path, delimiter=',', header='infer', index_col=None,dtype=np.float32)
[prot_age,prot_height,prot_weight,prot_ex_level,prot_abs_vo2_max] = protocol_inputs.iloc[(iter_num - 1),:]

#################################################################################################################################
# The basic protocol for resting is defined here
#################################################################################################################################
# The basic protocol for resting is defined here
class LPN_subject():

    def __init__(self, ht,wt,age,met,abs_vo2_max,name):
        self.ht = ht
        self.wt = wt
        self.age = age
        self.met = met
        self.abs_vo2_max = abs_vo2_max
        self.name = name
        
    def GenLPN_Flows(self):
        
        from pathlib import PurePath
        import os
        import numpy as np
        
        '''Declare variables (arbitrary values) for left-ventricular end systolic pressure and left ventricular ref volume. 
        EmaxLV and EmaxRV are needed, so they are calculated after NN is invoked.'''
        self.LVESP = 0
        self.Vlv0  = -200.0
        self.RVESP = 0
        self.Vrv0  = -200.0
        
        # Check to see if a modified all_inputs.csv file exists. If yes, delete to prevent conflicts in calculations
        fname = "all_inputs" + str(self.name) + ".csv"
        
        if os.path.exists(mod_inputs_dir/fname):
            try:
                os.remove(mod_inputs_dir/fname)
            except:
                print("Error while deleting file ", mod_inputs_dir/fname)
        else:
            print("NOTE: Existing modified all_inputs file not found. This is not an error, and can be ignored.")
        
        
        # This function calculates the change in flow rates during exercise
        #-----------------------------------------------------------------------------------------------------------------------
        # Calculate resting LPN outputs from Aseem's regression equations
        # Equations with exercise considered
        #sap       = 25.45 + 0.09*self.age + 0.4*self.ht + 0.24*self.wt + 5.57*self.met                 # Systolic Aortic pressure
        #dap       = 29.23 + 0.18*self.age + 0.15*self.ht + 0.18*self.wt + 1.02*self.met                # Diastolic Aortic Pressure
        self.co_ex = 0.71 - 0.06*self.age + 0.08*self.wt + 0.95*self.met                                # Cardiac Output
        self.hr    = -32.25 -0.36*self.age + 0.62*self.ht +0.13*self.wt + 9.27*self.met                 # Heart Rate

        # Only Resting Predictions
        self.lvedv_rest = -134.73 - 0.42*self.age + 1.04*self.ht + 1.25*self.wt                              # Left ventricular end diastolic volume
        self.lvesv_rest = -91.21 - 0.21*self.age + 0.68* self.ht + 0.36*self.wt                              # Left ventriculae end systolic volume
        self.rvedv_rest = -269.62 - 1.03*self.age + 2.44*self.ht + 0.63*self.wt                              # Right ventricular end diastolic volume
        self.rvesv_rest = -179.77 - 0.47*self.age  + 1.5*self.ht + 0.01*self.wt                              # Right ventricular end systolic volume
        self.co_rest    = (self.rvedv_rest  - self.rvesv_rest)*(self.hr/1000)                                     # corrected CO at rest from RV volumes
        
        #-----------------------------------------------------------------------------------------------------------------------
        # Calculate exercise ventricular volumes
        
        #Calculate change in LVESVI (Wyns et al,1982)----------------------------------------------------------------------------
        bsa_reg = ( (self.ht * self.wt)/3600 ) ** (0.5)
        LVESVI_ex = 0.000315*(self.hr**2) - 0.1868*self.hr + 41.98
        LVESVI_change = (30 - LVESVI_ex)/30
        
        # Calculate feature specific change in LVESVI
        self.LVESVI_init = self.lvesv_rest/bsa_reg
        self.LVESVI_feat = self.LVESVI_init*(1 - LVESVI_change)
        
        self.LVESV_ex = self.LVESVI_feat * bsa_reg
        
        # Calculate stroke volume based on exercise co
        self.sv_ex = ( self.co_ex * (1000/60) )/( self.hr/60)
        
        # LVEDV  = LVESV + SV
        self.LVEDV_ex = self.LVESV_ex + self.sv_ex
        
        #Calculate change in RVESVI (Wyns et al,1982)----------------------------------------------------------------------------
        RVESVI_ex = -0.001643*(self.hr**2) + 0.1649*self.hr + 60.49
        RVESVI_change = (64 - RVESVI_ex)/64
        
        # Calculate feature specific change in RVESVI
        self.RVESVI_init = self.rvesv_rest/bsa_reg
        self.RVESVI_feat = self.RVESVI_init*(1 - RVESVI_change)
        
        self.RVESV_ex = self.RVESVI_feat * bsa_reg
        
        # RVEDV  = RVESV + SV
        self.RVEDV_ex = self.RVESV_ex + self.sv_ex

        #-----------------------------------------------------------------------------------------------------------------------
        # Calculate change in splanchnic flow as a function of exercise intensity(Rowell 1993, Human Cardiovascular control)
        ex_intensity = (self.met*3.5)/self.abs_vo2_max
        flow_redn = 126 - (0.99*ex_intensity*100)

        # Resting Splanchnic blood flow ml/s
        self.rest_sp_flow = 0.27*(self.rvedv_rest  - self.rvesv_rest)

        # Exercise splanchnic flow
        self.ex_sp_flow = (flow_redn/100) * self.rest_sp_flow

        #-----------------------------------------------------------------------------------------------------------------------
        # Calculate change in renal flow during exercise (Rowell 1993, Human Cardiovascular control)

        # Resting Renal plasma flow ml/s. Assumption: normal hematocrit of 48% at rest
        self.rest_renal_flow = 0.22*(self.co_rest*1000)/60
        rest_renal_plasma = (1 - 0.48)*self.rest_renal_flow

        # Change in renal plasma flow during exercise
        plasma_flow_redn = -0.51*self.hr + 139

        # Exercise renal plasma volume
        ex_renal_plasma = (plasma_flow_redn * rest_renal_plasma)/100

        # Exercise renal flow
        self.ex_renal_flow = ex_renal_plasma / ( 1-0.48 )

        #-----------------------------------------------------------------------------------------------------------------------
        # Cerebral Flow (Rowell 1993, Human Cardiovascular control)

        # Total cerebral flow does not change significantly during exercise
        self.ex_cerebral_flow = 0.14*(self.co_rest*1000)/60

        #-----------------------------------------------------------------------------------------------------------------------
        # Coronary Flow
        # Determine left ventricular mass (deSimone 1992)
        bsa = 0.20247*(self.ht/100)**0.725*self.wt**0.425 #Dubois Formula (m^2)
        lvmass = 58*(bsa**1.5) #in grams

        # Left Ventricular Myocardial blood flow (Duncker,2008)
        lvmbf       = 0.016*self.hr - 0.3 #ml/(min.g)
        self.lvmbf_total = (lvmbf*lvmass)/60 #ml/s
        # CHECK: All flows in ml/s except CO which is l/min
 
    def GlobalVars(self):
        #-----------------------------------------------------------------------------------------------------------------------
        # Calculate TVR
        self.sap_rest = 25.45 + 0.09*self.age + 0.4*self.ht + 0.24*self.wt + 5.57                 # Systolic Aortic pressure (Resting)
        self.dap_rest = 29.23 + 0.18*self.age + 0.15*self.ht + 0.18*self.wt + 1.02                # Diastolic Aortic Pressure (Resting)
        # self.map_rest = (2/3)*self.dap_rest + (1/3)*self.sap_rest                                 # Resting mean art. press.
        self.rvedv_rest = -269.62 - 1.03*self.age + 2.44*self.ht + 0.63*self.wt                   # Right ventricular end diastolic volume
        self.rvesv_rest = -179.77 - 0.47*self.age  + 1.5*self.ht + 0.01*self.wt                   # Right ventricular end systolic volume
        self.co_rest    = (self.rvedv_rest  - self.rvesv_rest)*(self.hr/1000)                          # corrected CO at rest from RV volumes


        # MAP for exercise
        self.sap_ex   = 25.45 + 0.09*self.age + 0.4*self.ht + 0.24*self.wt + 5.57*self.met              # Systolic Aortic pressure
        self.dap_ex   = 29.23 + 0.18*self.age + 0.15*self.ht + 0.18*self.wt + 1.02*self.met             # Diastolic Aortic Pressure
        self.hr    = -32.25 -0.36*self.age + 0.62*self.ht +0.13*self.wt + 9.27*self.met                 # Heart Rate
        self.co_ex = 0.71 - 0.06*self.age + 0.08*self.wt + 0.95*self.met                                # Cardiac Output

        if (self.hr <= 120):
            self.ts = 0.5 - (0.2/60)*(120 - self.hr)*(60/self.hr)  # Systolic time ratio
        else:
            self.ts = 0.5

        self.map_ex = self.ts*self.sap_ex + (1 - self.ts)*self.dap_ex                                          # Results are similar to Moran et al.,1995
        self.map_rest = self.ts*self.sap_rest + (1 - self.ts)*self.dap_rest

        # PVR calculation
        # Estimate mean PA pressure during exercise (Kovacs,2012)
        if (self.age <= 50):
            if(self.co_ex < 7.2):
                self.mPA = 13.5
            elif (self.co_ex <= 13.67):
                self.mPA = 1.0448*self.co_ex + 5.977
            else:
                self.mPA = 0.757*self.co_ex + 9.902
        else:
            if (self.co_ex <= 6.2):
                self.mPA = 15.7
            elif(self.co_ex <= 9.689):
                self.mPA = 3.303*self.co_ex - 4.858
            else:
                self.mPA = 0.326*self.co_ex + 23.925

        #Estimate mean LA pressure during exercise (Kovacs,2012)
        if (self.age <= 50):
            if(self.co_ex < 7.74):
                self.lap_ex = 7.2
            elif (self.co_ex <= 13.72):
                self.lap_ex = 0.445*self.co_ex + 3.762
            else:
                self.lap_ex = 0.5151*self.co_ex + 2.8
        else:
            if (self.co_ex <= 6.2):
                self.lap_ex = 9.3
            elif(self.co_ex <= 9.71):
                self.lap_ex = 2.728*self.co_ex - 7.64 
            else:
                self.lap_ex = 0.2*self.co_ex + 16.91

        # Calculate systolic PA pressure
        self.sPAP = (self.mPA - 2)/0.61   # Formula from Chemla et al.,2004
        
        # Check if systolic PA pressure execeeds 50mmHg, if yes, cut off. (Based on Ferrara et al.,2018)
        if (self.sPAP>50):
            self.sPAP = 50.0
        
        # Calculate PA pulse pressure
        self.dPAP = (self.mPA - self.ts*self.sPAP)/(1-self.ts)
        self.PA_pulse_press = self.sPAP - self.dPAP
        
        # SVR calculation
        # Calculate right atrial pressure from lap (as pa wedge pressure) 
        lap_rest  = 7.2                                              # From Kovacs, 2012
        # rap_ex from Reeves, 1990. WARNING: Figure 4 in that paper shows WRONG reg eqn, checked with webplot digitizer
        self.rap_ex   = (self.lap_ex - 4.322)/1.15
        self.svr_rest = (self.map_rest - 3)/( self.co_rest*1000/60 )               # Assuming normal resting Right Atrial Press of 3 mmHg from Reeves paper
        self.svr_ex   = (self.map_ex - self.rap_ex)/(self.co_ex*1000/60)
        #rap_rest     = (lap_rest - 4.7)/1.45                                      # This is the wrong equation in the Reeves paper

        self.pvr_ex   = (self.mPA - self.lap_ex)/(self.co_ex*1000/60)                   # Checked against Wright, 2016
        tvr_ex        = self.pvr_ex + self.svr_ex
#         self.pvr_rest = (13.5 - lap_rest )/( self.co_rest*1000/60)                 # Matches with Wright,2016 (control, i.e, resting)
        self.pvr_rest = (self.mPA - lap_rest )/( self.co_rest*1000/60)                 # Matches with Wright,2016 (control, i.e, resting)
        tvr_rest      = self.svr_rest + self.pvr_rest

        #-----------------------------------------------------------------------------------------------------------------------
        # Reference TVR from Aseem's tuned LPN which was corrected. These values were checked on Feb 22nd 2022
        # map_ref  = 97.7519
        # cvp_ref  = 5.7643
        # mpap_ref = 11.7285
        # lap_ref  = 6.9398
        
        self.svr_ref = 1.337642514702847   # NOTE: Change if reference LPN is altered!
        self.pvr_ref = 0.099993257814268  # NOTE: Change if reference LPN is altered!
        tvr_ref = self.svr_ref + self.pvr_ref

        # Calculate the scaling factors
        self.tvrsf_rest = tvr_rest/tvr_ref
        self.tvrsf_ex   = tvr_ex/tvr_ref
        
        self.svrsf_rest = self.svr_rest/self.svr_ref
        self.svrsf_ex   = self.svr_ex/self.svr_ref        

        self.pvrsf_rest = self.pvr_rest/self.pvr_ref
        self.pvrsf_ex   = self.pvr_ex/self.pvr_ref
        
    def ResCapScale(self):
        import numpy as np
        import pandas as ps
        from pathlib import PurePath
        
        # First load the allinputs.csv file
        dpath = PurePath(ref_inputs_dir)
        inputs = ps.read_csv(dpath/"all_inputs.csv")
        
        # Filename of modified all_inputs.csv file
        fname2 = "all_inputs" + str(self.name) + ".csv"
        
        # Set up all the indices here
        SVR_indices     = np.array([166,169,171,177,178,185,186,190,191,192])
        PVR_indices     = np.array([195,196])
        SysCap_indices  = np.array([165,168,170,175,176,183,184,187,188,189])       # All systemic capacitances
        # spl_resistances = np.array([190,191,192])                                   # Splanchnic (Liver,Kidney,Intestine) resistances
        # spl_cap         = np.array([187,188,189])                                   # Splanchnic (Liver,Kidney,Intestine) capacitances
        large_art_res   = np.array([177, 178])                                      # Rthao and Rabao
        large_art_cap   = np.array([175,176])                                       # Cthao,Cabao
        
        
        # Check if resting or exercise
        if(self.met == 1):
            
            # Scale the reference resistances by calculated resting SVR and PVR factors
            inputs.iloc[ SVR_indices , 2 ] = inputs.iloc[ SVR_indices , 2 ]*self.svrsf_rest
            inputs.iloc[ PVR_indices , 2 ] = inputs.iloc[ PVR_indices , 2 ]*self.pvrsf_rest
            
            # Leg scaling for resting
            inputs.iloc[ 182, 2 ] = inputs.iloc[ 182, 2 ] * self.svrsf_rest
            
            # Now we use allometric scaling for ALL the capacitors 
            inputs.iloc[ SysCap_indices , 2 ] = inputs.iloc[ SysCap_indices , 2 ] * (self.tvrsf_rest ** (-4/3) )
            # Scale lung capacitors
            inputs.iloc[ 193 , 2 ]            = inputs.iloc[ 193 , 2 ] * (self.tvrsf_rest ** (-4/3) )
            inputs.iloc[ 194 , 2 ]            = inputs.iloc[ 194 , 2 ] * (self.tvrsf_rest ** (-4/3) )
            
#             # Scale lung capacitances
#             pul_scale = self.pvr_rest/self.pvr_ref
#             inputs.iloc[ 193, 2 ] = inputs.iloc[ 193, 2 ] * (pul_scale ** (-3/4) )      #scale_Clung1
#             inputs.iloc[ 194, 2 ] = inputs.iloc[ 194, 2 ] * (pul_scale ** (-3/4) )      #scale_Clung2

            # Calculate Raov using a linear relation. This may have to be changed for exercise
            # Raov = 0.01124*self.co_rest + 0.06671
            # Raov_base = inputs.iloc[ 207 , 2 ] * inputs.iloc[ 208 , 2 ]
            # self.Raov_scale = Raov/Raov_base
            # inputs.iloc[ 208 , 2 ] = inputs.iloc[ 208 , 2 ] * self.Raov_scale 
            # scale_Raov

            # Write the new allinputs_mod.csv file
            inputs.to_csv(mod_inputs_dir/fname2,index=False,header=True,float_format = "%.12f")
              
        elif(self.met <= 14):
            
            # Start with the attribute-specific RESTING resistances
            inputs.iloc[ SVR_indices , 2 ] = inputs.iloc[ SVR_indices , 2 ]*self.svrsf_rest
            inputs.iloc[ PVR_indices , 2 ] = inputs.iloc[ PVR_indices , 2 ]*self.pvrsf_rest
            
            # Now we use allometric scaling for the capacitors which is the same for resting and exercise
            inputs.iloc[ SysCap_indices , 2 ] = inputs.iloc[ SysCap_indices , 2 ] * (self.tvrsf_rest ** (-4/3) )
            inputs.iloc[ 193 , 2 ]            = inputs.iloc[ 193 , 2 ] * (self.tvrsf_rest ** (-4/3) )
            inputs.iloc[ 194 , 2 ]            = inputs.iloc[ 194 , 2 ] * (self.tvrsf_rest ** (-4/3) )
            
            # Modifications for exercise start here. NOTE: Execute GenLPN_Flows function in this class first!
            
            # # Splanchnic system
            # Rsp_ex       = (self.map_ex - self.rap_ex)/self.ex_sp_flow
            # Rsp_rest     = (self.map_rest - 3)/self.rest_sp_flow          #Assumed resting Right atrial pressure of 3mmHg
            # Rsp_scale    = Rsp_ex/Rsp_rest
            # print(f"Rsp_scale: {Rsp_scale}")
            # Csp_scale_ex = (Rsp_ex/Rsp_rest)**( -3/4 )
            
            # # Modify Splanchnic resistances and capacitances
            # inputs.iloc[ spl_resistances , 2 ] = inputs.iloc[ spl_resistances , 2 ] * Rsp_scale
            # inputs.iloc[ spl_cap , 2 ]         = inputs.iloc[ spl_cap , 2 ] * Csp_scale_ex
            
            # # Cerebral branch
            # Rcrb_ex                = (self.map_ex - self.rap_ex)/self.ex_cerebral_flow
            # Rcrb_ref               = inputs.iloc[ 166 , 2 ] * inputs.iloc[ 131 , 2 ]
            # Rcrb_scale             = Rcrb_ex/Rcrb_ref
            # # Modify cerebral resistances and capacitances
            # inputs.iloc[ 166 , 2 ] = Rcrb_scale
            # Ccrb_scale_ex          = (Rcrb_scale)**( -3/4 )
            # inputs.iloc[ 165 , 2 ] = Ccrb_scale_ex
            
            # Scale large artery resistances and capacitances
            inputs.iloc[ large_art_res , 2 ] = inputs.iloc[ large_art_res , 2 ] * (self.svr_ex/self.svr_rest)
            inputs.iloc[ large_art_cap , 2 ] = inputs.iloc[ large_art_cap , 2 ] * ((self.svr_ex/self.svr_rest) ** (-3/4) )
            
            # Tuning the leg resistances
            Rcrb = inputs.iloc[ 166, 2 ]*inputs.iloc[ 131, 2 ]
            Rh   = inputs.iloc[ 169, 2 ]*inputs.iloc[ 134, 2 ]
            
            Rleg = inputs.iloc[ 182, 2 ]*inputs.iloc[ 147, 2 ]
            # Rlegv = inputs.iloc[ 166, 2 ]*inputs.iloc[ 131, 2 ]
            # Rlegc = inputs.iloc[ 166, 2 ]*inputs.iloc[ 131, 2 ]
            
            Ri = inputs.iloc[ 192, 2 ]*inputs.iloc[ 157, 2 ]
            Rk = inputs.iloc[ 191, 2 ]*inputs.iloc[ 156, 2 ]
            Rl = inputs.iloc[ 190, 2 ]*inputs.iloc[ 155, 2 ]
            
            Rthao  = inputs.iloc[ 177, 2 ]*inputs.iloc[ 142, 2 ]
            Rabao  = inputs.iloc[ 178, 2 ]*inputs.iloc[ 143, 2 ]
            Rabivc = inputs.iloc[ 185, 2 ]*inputs.iloc[ 152, 2 ]
            Rthivc = inputs.iloc[ 186, 2 ]*inputs.iloc[ 151, 2 ]
            Rsvc   = inputs.iloc[ 171, 2 ]*inputs.iloc[ 136, 2 ]
            
            # Need to have Rla for the y-delta transform later. Calculate from the fraction
            Rla = Rl * inputs.iloc[ 202, 2 ]
            Rlv = Rl - Rla
            
            Rall = Rla + Rabao + Ri

            R1= (Rla*Rabao)/Rall
            R2= (Rla*Ri)/Rall
            R3= (Ri*Rabao)/Rall
            print(f"R1: {R1}")
            print(f"R2: {R2}")
            print(f"R3: {R3}")
            
            # Initial large value
            diff = 5000
            # Now we have to loop through the Rleg scaling to find the right factor
            for scale in np.linspace(1.05, -5.0, num=10000):

                Rleg_scale= (Rleg + Rabivc)*scale
#                 Rh = Rh*scale

                Rtemp = 1/( 1/(R2 + Rlv)  +  1/(R3 + Rleg_scale) ) + R1
                Rtemp = 1/(  1/Rtemp  +  1/(Rk)  )

                # Final results of resistances
#                 LBR= Rthao + Rtemp + Rthivc

                LBR    = Rtemp + Rthao + Rthivc
                UBR    = 1/(1/Rcrb + 1/Rh) + Rsvc
                SVRnew = 1/(1/LBR + 1/UBR)

                diff2 = abs( SVRnew - self.svr_ex)

                if (diff2 > diff):
                    print(f"SVR new: {SVRnew}")
                    print(f"SVR old: {self.svr_rest}")
                    print(f"diff2: {diff2}")
                    print(f"diff: {diff}")
                    print(f"scale: {scale}")
                    print(f"Rtemp: {Rtemp}")
                    break

                diff  = abs( SVRnew - self.svr_ex)
            
            print(f"diff2:{diff2}")
            
            # Assign the scaling factor to an instance variable so we can access it later
            self.leg_res_scale = scale
            
            # Now perform the actual leg scaling. NOTE: For now not including the capacitor scaling since it was skipped in The Fontan code
            inputs.iloc[ 182, 2 ] = inputs.iloc[ 182, 2 ] * scale
            inputs.iloc[ 185, 2 ] = inputs.iloc[ 185, 2 ] * scale
#             inputs.iloc[ 186, 2 ] = inputs.iloc[ 186, 2 ] * scale
            
            # Modify the pulmonary resistances to match desired PVR            
            R_branch1 = inputs.iloc[ 160, 2 ] * inputs.iloc[ 195, 2 ]
            R_branch2 = inputs.iloc[ 161, 2 ] * inputs.iloc[ 196, 2 ]
            Rtotal    = 1/(1/R_branch1 + 1/R_branch2)
            pul_scale = self.pvr_ex/Rtotal            
#            pul_scale = Rtotal/self.pvr_ex
            
            # Actual scaling of pulmonary resitances
            inputs.iloc[ 195, 2 ] = inputs.iloc[ 195, 2 ] * pul_scale       #scale_Rlung1
            inputs.iloc[ 196, 2 ] = inputs.iloc[ 196, 2 ] * pul_scale       #scale_Rlung2
            
            # Scale capacitances
            inputs.iloc[ 193, 2 ] = inputs.iloc[ 193, 2 ] * (pul_scale ** (-3/4) )      #scale_Clung1
            inputs.iloc[ 194, 2 ] = inputs.iloc[ 194, 2 ] * (pul_scale ** (-3/4) )      #scale_Clung2
            
            # Write the new allinputs_mod.csv file
            inputs.to_csv(mod_inputs_dir/fname2,index=False,header=True,float_format = "%.12f")
            
        else:
            print("ERROR: Entered met level either exceeds allowable value of 14 or is below 1.")

# Run the protocol for a test subject, one run necessary for predicting Vlv0
sub1 = LPN_subject(prot_height,prot_weight,prot_age,prot_ex_level,prot_abs_vo2_max,iter_num)
sub1.GenLPN_Flows()
sub1.GlobalVars()
sub1.ResCapScale()

# Set up the data for the neural network
index = range(1) # Only one row of inputs for now
# columns = ['HR','Mean Aortic Pressure','Systolic Aortic Pressure','Pulse Pressure','SVR','Max LV Volume','Min LV Volume','Max RV volume',\
# 'Min RV Volume','Mean PA Pressure','Systolic PA pressure','PA Pulse pressure','PVR','LVEF','RVEF','Systemic capacitor scale',\
# 'Leg capacitor scale','Leg_Res_Scale','tas_percent','t1_percent','Vra0','Vla0','CcsaR','csaR','dsaR','CcsaL','csaL','dsaL']

# Columns for resting
columns = ['HR','Mean Aortic Pressure','Systolic Aortic Pressure','Diastolic_Pressure','Pulse Pressure','scale_SVR','Max LV Volume','Min LV Volume',\
           'Max RV volume','Min RV Volume','Mean PA Pressure','Systolic PA pressure','PA Pulse pressure','scale_PVR','LVEF','RVEF',\
           'Systemic capacitor scale','Mean_RA_Pressure','Mean_LA_Pressure','skewness']
x_trim2 = ps.DataFrame(0,index=index,columns = columns, dtype=float)

x_trim2.columns = [c.replace(' ', '_') for c in x_trim2.columns] # Replace all the spaces in the column names with underscore for easier access
# x_trim2.tail()

hr_scale = sub1.hr/60
x_trim2 = x_trim2.assign(HR=hr_scale)

# Set pulomnary pressures here
mean_PA_press = 13.5
sys_PA_press = 17.0
pa_dp = ( mean_PA_press - sub1.ts*(sys_PA_press) ) / (1 - sub1.ts)
pa_pp = sys_PA_press - pa_dp

if (sub1.met == 1):
    x_trim2 = x_trim2.assign(Mean_Aortic_Pressure=sub1.map_rest)
    x_trim2 = x_trim2.assign(Systolic_Aortic_Pressure= sub1.sap_rest) # NOTE: 1.1* added to original protocol script!
    x_trim2 = x_trim2.assign(Diastolic_Pressure=sub1.dap_rest)
    pp = sub1.sap_rest - sub1.dap_rest # Pulse pressure
    x_trim2 = x_trim2.assign(Pulse_Pressure=pp)
    x_trim2 = x_trim2.assign(scale_SVR=sub1.svrsf_rest)
    x_trim2 = x_trim2.assign(Max_LV_Volume=sub1.lvesv_rest + (sub1.rvedv_rest - sub1.rvesv_rest))
    x_trim2 = x_trim2.assign(Min_LV_Volume=sub1.lvesv_rest)
    x_trim2 = x_trim2.assign(Max_RV_volume=sub1.rvedv_rest)
    x_trim2 = x_trim2.assign(Min_RV_Volume=sub1.rvesv_rest)
    x_trim2 = x_trim2.assign(Mean_PA_Pressure=mean_PA_press)         
    x_trim2 = x_trim2.assign(Systolic_PA_pressure=sys_PA_press)
    # pa_pp = sub1.sPAP - sub1.mPA # PA Pulse pressure
    x_trim2 = x_trim2.assign(PA_Pulse_pressure=pa_pp)
    x_trim2 = x_trim2.assign(scale_PVR=sub1.pvrsf_rest)
    LVEF = (sub1.lvedv_rest - sub1.lvesv_rest)/sub1.lvedv_rest
    RVEF = (sub1.rvedv_rest - sub1.rvesv_rest)/sub1.rvedv_rest
    x_trim2 = x_trim2.assign(LVEF=LVEF)
    x_trim2 = x_trim2.assign(RVEF=RVEF)
    x_trim2 = x_trim2.assign(Systemic_capacitor_scale=sub1.tvrsf_rest ** (-4/3))
    x_trim2 = x_trim2.assign(Mean_RA_Pressure=3) # Constant Values for resting
    x_trim2 = x_trim2.assign(Mean_LA_Pressure=7.1) # Constant Values for resting
    x_trim2 = x_trim2.assign(skewness = 1.21) 
    
elif (sub1.met <= 14):
    x_trim2 = x_trim2.assign(Mean_Aortic_Pressure=sub1.map_ex)
    x_trim2 = x_trim2.assign(Systolic_Aortic_Pressure= sub1.sap_ex)
    x_trim2 = x_trim2.assign(Diastolic_Pressure=sub1.dap_ex)
    pp = sub1.sap_ex - sub1.dap_ex # Pulse pressure
    x_trim2 = x_trim2.assign(Pulse_Pressure=pp)
    x_trim2 = x_trim2.assign(scale_SVR=sub1.svrsf_ex)
    x_trim2 = x_trim2.assign(Max_LV_Volume=sub1.LVEDV_ex)
    x_trim2 = x_trim2.assign(Min_LV_Volume=sub1.LVESV_ex)
    x_trim2 = x_trim2.assign(Max_RV_volume=sub1.RVEDV_ex)
    x_trim2 = x_trim2.assign(Min_RV_Volume=sub1.RVESV_ex)
    x_trim2 = x_trim2.assign(Mean_PA_Pressure=sub1.mPA)         # mPA calculation accounts for resting and exercise,
    x_trim2 = x_trim2.assign(Systolic_PA_pressure=(sub1.sPAP))    # sPAP calculation accounts for resting and exercise
    # pa_pp = sub1.sPAP - sub1.mPA # PA Pulse pressure
    x_trim2 = x_trim2.assign(PA_Pulse_pressure=sub1.PA_pulse_press)
    x_trim2 = x_trim2.assign(scale_PVR=sub1.pvrsf_ex)
    LVEF = (sub1.LVEDV_ex - sub1.LVESV_ex)/sub1.LVEDV_ex
    RVEF = (sub1.RVEDV_ex - sub1.RVESV_ex)/sub1.RVEDV_ex
    x_trim2 = x_trim2.assign(LVEF=LVEF)
    x_trim2 = x_trim2.assign(RVEF=RVEF)
    x_trim2 = x_trim2.assign(Systemic_capacitor_scale=sub1.tvrsf_rest ** (-4/3)) # Remember this factor is for allometric scaling, so it should be the resting value!
    x_trim2 = x_trim2.assign(Mean_RA_Pressure=sub1.rap_ex)
    x_trim2 = x_trim2.assign(Mean_LA_Pressure=sub1.lap_ex)
    x_trim2 = x_trim2.assign(skewness = 1.21)

else:
    print("ERROR: Entered met level either exceeds allowable value of 14 or is below 1.")


# Load the neural network and scaler data here
model = tf.keras.models.load_model(nn_path)
scaler_x = joblib.load(scaler_x_path)
scaler_y = joblib.load(scaler_y_path)

# Scale the NN inputs before supplying them to the NN
input_x = np.array(x_trim2.iloc[:,0:num_predictors])
xscale2 = scaler_x.transform(input_x)

# Call the neural network to get the predictions
test_predict = model.predict(xscale2)
# Do an inverse transform of the NN output to get the actual scaling factors
yhat = scaler_y.inverse_transform(test_predict)

# Save the neural network predictions and other quantities to the allinputs.csv file.
# NOTE: Several quantities such as scale_Lao have been removed since they arent predicted well by the network.

# First load the allinputs.csv file
# dpath = PurePath("/home/akashg/gen_LPN_protocol/")
fname3 = "all_inputs" + str(sub1.name) + ".csv"
inputs = ps.read_csv(mod_inputs_dir/fname3)

#Assign the scaling factors here
inputs.iloc[  94 , 2 ] = inputs.iloc[  94 , 2 ] * input_x[0,0]      # scale_hr
inputs.iloc[ 106 , 2 ] = inputs.iloc[ 106 , 2 ] * yhat[0,0]         # scale_EmaxRV
inputs.iloc[ 107 , 2 ] = inputs.iloc[ 107 , 2 ] * yhat[0,1]         # scale_EoffsetRV
inputs.iloc[ 108 , 2 ] = inputs.iloc[ 108 , 2 ] * yhat[0,2]         # scale_EmaxLV
inputs.iloc[ 109 , 2 ] = inputs.iloc[ 109 , 2 ] * yhat[0,3]         # scale_EoffsetlV
# inputs.iloc[ 110 , 2 ] = inputs.iloc[ 110 , 2 ] * yhat[0,4]         # scale_Vrv0
# inputs.iloc[ 111 , 2 ] = inputs.iloc[ 111 , 2 ] * yhat[0,5]         # scale_Vlv0
# inputs.iloc[ 112 , 2 ] = inputs.iloc[ 112 , 2 ] * yhat[0,6]         # scale_AVCouple
# inputs.iloc[ 120 , 2 ] = inputs.iloc[ 120 , 2 ] * yhat[0,7]         # scale_Lao
inputs.iloc[ 172 , 2 ] = inputs.iloc[ 172 , 2 ] * yhat[0,4]         # scale_Cao
# inputs.iloc[ 118 , 2 ] = inputs.iloc[ 118 , 2 ] * yhat[0,9]         # scale_Lpul
# inputs.iloc[ 193 , 2 ] = inputs.iloc[ 193 , 2 ] * yhat[0,5]        # scale_Clung1, same scaling factor for both
# inputs.iloc[ 194 , 2 ] = inputs.iloc[ 194 , 2 ] * yhat[0,5]        # scale_Clung2, same scaling factor for both
inputs.iloc[ 0 , 2 ] = inputs.iloc[ 0 , 2 ] * yhat[0,5]        # scale_init

inputs.iloc[ 208 , 2 ] = inputs.iloc[ 208 , 2 ] * yhat[0,6]  #scale_Raov

# Calculate Vlv0 from MongeGarcia et al.,2019
# To calculate this create a dummy object(sub1_vlv0) with the same properties but MET = 2
# sub1_vlv0 = LPN_subject(sub1.ht,sub1.wt,sub1.age,10,sub1.abs_vo2_max,"sub1_vlv0")
# sub1_vlv0.GenLPN_Flows()
# sub1_vlv0.GlobalVars()
# # sub1_vlv0.ResCapScale()


# SV = sub1_vlv0.RVEDV_ex-sub1_vlv0.RVESV_ex
# sub1_vlv0.LVESP = ( sub1_vlv0.map_ex/0.8721 ) - (SV*0.051565)
# # Use the dummy objects values to calculate Vlv0 for the actual subject
# EmaxLV_dummy = inputs.iloc[ 108 , 2 ] * yhat[0,2] * inputs.iloc[ 75 , 2 ]
# sub1.Vlv0 = sub1_vlv0.LVESV_ex - (sub1_vlv0.LVESP/EmaxLV_dummy )
# Vlv0_ref = inputs.iloc[ 111 , 2 ] * inputs.iloc[ 78 , 2 ]
# Vlv0_scale = sub1.Vlv0/Vlv0_ref
# inputs.iloc[ 111 , 2 ] = inputs.iloc[ 111 , 2 ] * Vlv0_scale        # scale_Vlv0

if (sub1.met == 1):
    # Calculate Vlv0 from MongeGarcia et al.,2019 using resting predictions
    SV = sub1.rvedv_rest-sub1.rvesv_rest
    sub1.LVESP = ( sub1.map_rest/0.8721 ) - (SV*0.051565)
    EmaxLV_dummy = inputs.iloc[ 75 , 2 ] * inputs.iloc[ 108 , 2 ]
    sub1.Vlv0 = ( sub1.lvesv_rest - (sub1.LVESP/EmaxLV_dummy) ) + 3.5
    Vlv0_ref = inputs.iloc[ 78 , 2 ] # From the allinputs.csv file
    Vlv0_scale = sub1.Vlv0/Vlv0_ref
    inputs.iloc[ 111 , 2 ] = inputs.iloc[ 111 , 2 ] * Vlv0_scale        # scale_Vlv0

    # Estimate Vrv0 by assuming RV Ees/Ea = 2.0 at rest (Kuehne,2004)
    # EmaxRV_dummy = inputs.iloc[ 106 , 2 ] * inputs.iloc[ 73 , 2 ]
    # EaRV = EmaxRV_dummy/2.0
    # sub1.RVESP = EaRV*SV

    # Estimate RVESP as 2/3 of systolic + mean PA pressure
    EmaxRV_dummy = inputs.iloc[ 106 , 2 ] * inputs.iloc[ 73 , 2 ]
    sub1.RVESP = (sub1.mPA + sub1.sPAP)*(2/3)
    sub1.Vrv0 = (sub1.rvesv_rest - (sub1.RVESP/EmaxRV_dummy)) + 17 # +17 added due to consistent under prediction of ~14ml
    Vrv0_ref = inputs.iloc[ 77 , 2 ]
    Vrv0_scale = sub1.Vrv0/Vrv0_ref
    inputs.iloc[ 110 , 2 ] = inputs.iloc[ 110 , 2 ] * Vrv0_scale        # scale_Vrv0
    
elif (sub1.met <= 14):
    # Calculate Vlv0 from MongeGarcia et al.,2019 using resting predictions
    SV = sub1.RVEDV_ex-sub1.RVESV_ex
    sub1.LVESP = ( sub1.map_ex/0.8721 ) - (SV*0.051565)
    EmaxLV_dummy = inputs.iloc[ 75 , 2 ] * inputs.iloc[ 108 , 2 ]
    sub1.Vlv0 = sub1.LVESV_ex - (sub1.LVESP/EmaxLV_dummy)
    Vlv0_ref = inputs.iloc[ 78 , 2 ] # From the allinputs.csv file
    Vlv0_scale = sub1.Vlv0/Vlv0_ref
    inputs.iloc[ 111 , 2 ] = inputs.iloc[ 111 , 2 ] * Vlv0_scale        # scale_Vlv0

    # Estimate Vrv0 by assuming RV Ees/Ea = 2.0 at rest (Kuehne,2004)
    # EmaxRV_dummy = inputs.iloc[ 106 , 2 ] * inputs.iloc[ 73 , 2 ]
    # EaRV = EmaxRV_dummy/2.0
    # sub1.RVESP = EaRV*SV

    # Estimate RVESP as 2/3 of systolic + mean PA pressure
    EmaxRV_dummy = inputs.iloc[ 106 , 2 ] * inputs.iloc[ 73 , 2 ]
    sub1.RVESP = (sub1.mPA + sub1.sPAP)*(2/3)
    sub1.Vrv0 = (sub1.RVESV_ex - (sub1.RVESP/EmaxRV_dummy)) + 15 # +15 added due to consistent under prediction of ~12ml
    Vrv0_ref = inputs.iloc[ 77 , 2 ]
    Vrv0_scale = sub1.Vrv0/Vrv0_ref
    inputs.iloc[ 110 , 2 ] = inputs.iloc[ 110 , 2 ] * Vrv0_scale        # scale_Vrv0
else:
    print("ERROR: Entered met level either exceeds allowable value of 14 or is below 1.")

# Write the new allinputs_mod.csv file
inputs.to_csv(mod_inputs_dir/fname3,index=False,header=True,float_format = "%.12f")
print(f"Vlv0: {sub1.Vlv0}")
# print(f"Vlv0_scale: {Vlv0_scale}")
# print(f"EmaxLV for Vlv0 calculation: {EmaxLV_dummy}")
print(f"Vrv0: {sub1.Vrv0}")
# print(f"Vrv0_scale: {Vrv0_scale}")
# print(f"EmaxRV for Vrv0 calculation: {EmaxRV_dummy}")
# print(f"Height: {sub1.ht}")
# print(f"Weight: {sub1.wt}")
# print(f"Age: {sub1.age}")