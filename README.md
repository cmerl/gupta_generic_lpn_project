## The Generic LPN Project Git Repository
This is the Git repository for the code relevant to the publication:

**What do you need to run this protocol?**

Here, we assume that you have access to Clemson's Palmetto Supercomputing Cluster, or a Linux system which has tensorflow-gpu installed. 
To install TensorFlow on your Palmetto account follow the instructions here : https://www.palmetto.clemson.edu/palmetto/software/software/#tensorflow , or contact the Palmetto admin team for help.

This code has been tested and works with the folloiwing modules available on the Palmetto cluster:

1. cuda/10.2.89-gcc/8.3.1
2. cudnn/7.6.5.32-10.2-linux-x64-gcc/8.3.1-cuda10_2
3. anaconda3/2019.10-gcc/8.3.1
4. gcc/9.3.0

---
## How do I provide inputs to this protocol?

To run, this protocol needs the subject age, height, and weight. You can enter these details on the second line of the input.csv file. It is also necessary to provide the name of the conda environment 
in which you have TensorFlow installed.  Please enter the name of the environment on line 17 of the run_protocol.pbs file.

---
## How do you actually run this protocol?

To run the protocol on you Palmetto acount you need to submit the "run_protocol.pbs" file as a batch job. Do this by typing the following command:

```qsub run_protocol.pbs```

Running this command will launch the protocol with the inputs provided in the input.csv file. The bi-ventricular LPN will also be run automatically, and the results will be saved in a new folder 
called "data" in a file named AllData.csv.
---
## Description of folders and files in this repository

1. Neural_Network : This folder contains the all the neural network related files needed to run the protocol including the scalers.
2. data : If the default name for the output directory has not been changed in line 11 of "run_protocol.pbs", this folder will contain the output of running the Bi-Ventricular LPN with the protocol.
3. ref_all_inputs : Contains the refernce all_inputs.csv file. Normally, there should be no need to modify this.
4. ref_files : Contains the fortran and python files for the Bi-Ventricular LPN.
5. gen_input.py : This python script generates the parameters.f90 and initVals.f90 files which are needed to run the LPN.
6. input.csv: Contains the age, height, and weight data for the subject to be simulated.
7. protocol_script.py : This file which actually contains the protocol.
8. run_protocol.pbs : This file is submitted to start a batch job on Palmetto to actually run the protocol and the LPN.